package 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	[SWF(frameRate = "60", backgroundColor = "0xffffff")]
	
	/** based on the following video tutorial: http://youtu.be/WcAV9rOGjxw
	*/
	
	public class ProxyPatternDemo extends Sprite 
	{
		private var drone:Zerg;
		
		public function ProxyPatternDemo():void 
		{
			stage.addEventListener(MouseEvent.CLICK, onMouseClick);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		
		}
		
		private function onKeyDown(e:KeyboardEvent):void 
		{
			drone = new Cocoon();
			addChild(DisplayObject(drone));
		}
		
		private function onMouseClick(e:MouseEvent):void 
		{
			drone.move(mouseX, mouseY);
		}		
	}	
}